package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdTypeTemplate;
import com.wj.ecstore.pojo.JdTypeTemplateExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdTypeTemplateMapper {
    int countByExample(JdTypeTemplateExample example);

    int deleteByExample(JdTypeTemplateExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdTypeTemplate record);

    int insertSelective(JdTypeTemplate record);

    List<JdTypeTemplate> selectByExample(JdTypeTemplateExample example);

    JdTypeTemplate selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdTypeTemplate record, @Param("example") JdTypeTemplateExample example);

    int updateByExample(@Param("record") JdTypeTemplate record, @Param("example") JdTypeTemplateExample example);

    int updateByPrimaryKeySelective(JdTypeTemplate record);

    int updateByPrimaryKey(JdTypeTemplate record);
}