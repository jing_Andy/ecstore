package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdGoodsDesc;
import com.wj.ecstore.pojo.JdGoodsDescExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdGoodsDescMapper {
    int countByExample(JdGoodsDescExample example);

    int deleteByExample(JdGoodsDescExample example);

    int deleteByPrimaryKey(Long goodsId);

    int insert(JdGoodsDesc record);

    int insertSelective(JdGoodsDesc record);

    List<JdGoodsDesc> selectByExample(JdGoodsDescExample example);

    JdGoodsDesc selectByPrimaryKey(Long goodsId);

    int updateByExampleSelective(@Param("record") JdGoodsDesc record, @Param("example") JdGoodsDescExample example);

    int updateByExample(@Param("record") JdGoodsDesc record, @Param("example") JdGoodsDescExample example);

    int updateByPrimaryKeySelective(JdGoodsDesc record);

    int updateByPrimaryKey(JdGoodsDesc record);
}