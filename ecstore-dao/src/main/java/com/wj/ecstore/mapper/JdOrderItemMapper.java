package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdOrderItem;
import com.wj.ecstore.pojo.JdOrderItemExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdOrderItemMapper {
    int countByExample(JdOrderItemExample example);

    int deleteByExample(JdOrderItemExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdOrderItem record);

    int insertSelective(JdOrderItem record);

    List<JdOrderItem> selectByExample(JdOrderItemExample example);

    JdOrderItem selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdOrderItem record, @Param("example") JdOrderItemExample example);

    int updateByExample(@Param("record") JdOrderItem record, @Param("example") JdOrderItemExample example);

    int updateByPrimaryKeySelective(JdOrderItem record);

    int updateByPrimaryKey(JdOrderItem record);
}