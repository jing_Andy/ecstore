package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdUser;
import com.wj.ecstore.pojo.JdUserExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdUserMapper {
    int countByExample(JdUserExample example);

    int deleteByExample(JdUserExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdUser record);

    int insertSelective(JdUser record);

    List<JdUser> selectByExample(JdUserExample example);

    JdUser selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdUser record, @Param("example") JdUserExample example);

    int updateByExample(@Param("record") JdUser record, @Param("example") JdUserExample example);

    int updateByPrimaryKeySelective(JdUser record);

    int updateByPrimaryKey(JdUser record);
}