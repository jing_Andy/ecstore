package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdFreightTemplate;
import com.wj.ecstore.pojo.JdFreightTemplateExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdFreightTemplateMapper {
    int countByExample(JdFreightTemplateExample example);

    int deleteByExample(JdFreightTemplateExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdFreightTemplate record);

    int insertSelective(JdFreightTemplate record);

    List<JdFreightTemplate> selectByExample(JdFreightTemplateExample example);

    JdFreightTemplate selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdFreightTemplate record, @Param("example") JdFreightTemplateExample example);

    int updateByExample(@Param("record") JdFreightTemplate record, @Param("example") JdFreightTemplateExample example);

    int updateByPrimaryKeySelective(JdFreightTemplate record);

    int updateByPrimaryKey(JdFreightTemplate record);
}