package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdGoods;
import com.wj.ecstore.pojo.JdGoodsExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdGoodsMapper {
    int countByExample(JdGoodsExample example);

    int deleteByExample(JdGoodsExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdGoods record);

    int insertSelective(JdGoods record);

    List<JdGoods> selectByExample(JdGoodsExample example);

    JdGoods selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdGoods record, @Param("example") JdGoodsExample example);

    int updateByExample(@Param("record") JdGoods record, @Param("example") JdGoodsExample example);

    int updateByPrimaryKeySelective(JdGoods record);

    int updateByPrimaryKey(JdGoods record);
}