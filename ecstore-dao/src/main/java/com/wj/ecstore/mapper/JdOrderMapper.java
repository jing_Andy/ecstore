package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdOrder;
import com.wj.ecstore.pojo.JdOrderExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdOrderMapper {
    int countByExample(JdOrderExample example);

    int deleteByExample(JdOrderExample example);

    int deleteByPrimaryKey(Long orderId);

    int insert(JdOrder record);

    int insertSelective(JdOrder record);

    List<JdOrder> selectByExample(JdOrderExample example);

    JdOrder selectByPrimaryKey(Long orderId);

    int updateByExampleSelective(@Param("record") JdOrder record, @Param("example") JdOrderExample example);

    int updateByExample(@Param("record") JdOrder record, @Param("example") JdOrderExample example);

    int updateByPrimaryKeySelective(JdOrder record);

    int updateByPrimaryKey(JdOrder record);
}