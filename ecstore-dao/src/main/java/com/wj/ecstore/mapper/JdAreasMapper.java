package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdAreas;
import com.wj.ecstore.pojo.JdAreasExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdAreasMapper {
    int countByExample(JdAreasExample example);

    int deleteByExample(JdAreasExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(JdAreas record);

    int insertSelective(JdAreas record);

    List<JdAreas> selectByExample(JdAreasExample example);

    JdAreas selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") JdAreas record, @Param("example") JdAreasExample example);

    int updateByExample(@Param("record") JdAreas record, @Param("example") JdAreasExample example);

    int updateByPrimaryKeySelective(JdAreas record);

    int updateByPrimaryKey(JdAreas record);
}