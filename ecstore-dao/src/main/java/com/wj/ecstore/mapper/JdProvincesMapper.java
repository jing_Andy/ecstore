package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdProvinces;
import com.wj.ecstore.pojo.JdProvincesExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdProvincesMapper {
    int countByExample(JdProvincesExample example);

    int deleteByExample(JdProvincesExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(JdProvinces record);

    int insertSelective(JdProvinces record);

    List<JdProvinces> selectByExample(JdProvincesExample example);

    JdProvinces selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") JdProvinces record, @Param("example") JdProvincesExample example);

    int updateByExample(@Param("record") JdProvinces record, @Param("example") JdProvincesExample example);

    int updateByPrimaryKeySelective(JdProvinces record);

    int updateByPrimaryKey(JdProvinces record);
}