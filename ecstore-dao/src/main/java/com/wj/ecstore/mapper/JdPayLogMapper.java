package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdPayLog;
import com.wj.ecstore.pojo.JdPayLogExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdPayLogMapper {
    int countByExample(JdPayLogExample example);

    int deleteByExample(JdPayLogExample example);

    int deleteByPrimaryKey(String outTradeNo);

    int insert(JdPayLog record);

    int insertSelective(JdPayLog record);

    List<JdPayLog> selectByExample(JdPayLogExample example);

    JdPayLog selectByPrimaryKey(String outTradeNo);

    int updateByExampleSelective(@Param("record") JdPayLog record, @Param("example") JdPayLogExample example);

    int updateByExample(@Param("record") JdPayLog record, @Param("example") JdPayLogExample example);

    int updateByPrimaryKeySelective(JdPayLog record);

    int updateByPrimaryKey(JdPayLog record);
}