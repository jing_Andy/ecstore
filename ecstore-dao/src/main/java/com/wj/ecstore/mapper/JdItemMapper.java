package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdItem;
import com.wj.ecstore.pojo.JdItemExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdItemMapper {
    int countByExample(JdItemExample example);

    int deleteByExample(JdItemExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdItem record);

    int insertSelective(JdItem record);

    List<JdItem> selectByExample(JdItemExample example);

    JdItem selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdItem record, @Param("example") JdItemExample example);

    int updateByExample(@Param("record") JdItem record, @Param("example") JdItemExample example);

    int updateByPrimaryKeySelective(JdItem record);

    int updateByPrimaryKey(JdItem record);
}