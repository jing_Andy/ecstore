package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdBrand;
import com.wj.ecstore.pojo.JdBrandExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdBrandMapper {
    int countByExample(JdBrandExample example);

    int deleteByExample(JdBrandExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdBrand record);

    int insertSelective(JdBrand record);

    List<JdBrand> selectByExample(JdBrandExample example);

    JdBrand selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdBrand record, @Param("example") JdBrandExample example);

    int updateByExample(@Param("record") JdBrand record, @Param("example") JdBrandExample example);

    int updateByPrimaryKeySelective(JdBrand record);

    int updateByPrimaryKey(JdBrand record);
}