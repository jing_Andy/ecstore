package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdCities;
import com.wj.ecstore.pojo.JdCitiesExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdCitiesMapper {
    int countByExample(JdCitiesExample example);

    int deleteByExample(JdCitiesExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(JdCities record);

    int insertSelective(JdCities record);

    List<JdCities> selectByExample(JdCitiesExample example);

    JdCities selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") JdCities record, @Param("example") JdCitiesExample example);

    int updateByExample(@Param("record") JdCities record, @Param("example") JdCitiesExample example);

    int updateByPrimaryKeySelective(JdCities record);

    int updateByPrimaryKey(JdCities record);
}