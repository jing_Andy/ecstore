package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdContentCategory;
import com.wj.ecstore.pojo.JdContentCategoryExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdContentCategoryMapper {
    int countByExample(JdContentCategoryExample example);

    int deleteByExample(JdContentCategoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdContentCategory record);

    int insertSelective(JdContentCategory record);

    List<JdContentCategory> selectByExample(JdContentCategoryExample example);

    JdContentCategory selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdContentCategory record, @Param("example") JdContentCategoryExample example);

    int updateByExample(@Param("record") JdContentCategory record, @Param("example") JdContentCategoryExample example);

    int updateByPrimaryKeySelective(JdContentCategory record);

    int updateByPrimaryKey(JdContentCategory record);
}