package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdSpecification;
import com.wj.ecstore.pojo.JdSpecificationExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdSpecificationMapper {
    int countByExample(JdSpecificationExample example);

    int deleteByExample(JdSpecificationExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdSpecification record);

    int insertSelective(JdSpecification record);

    List<JdSpecification> selectByExample(JdSpecificationExample example);

    JdSpecification selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdSpecification record, @Param("example") JdSpecificationExample example);

    int updateByExample(@Param("record") JdSpecification record, @Param("example") JdSpecificationExample example);

    int updateByPrimaryKeySelective(JdSpecification record);

    int updateByPrimaryKey(JdSpecification record);
}