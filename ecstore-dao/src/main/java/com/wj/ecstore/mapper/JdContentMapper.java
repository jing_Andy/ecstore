package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdContent;
import com.wj.ecstore.pojo.JdContentExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdContentMapper {
    int countByExample(JdContentExample example);

    int deleteByExample(JdContentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdContent record);

    int insertSelective(JdContent record);

    List<JdContent> selectByExample(JdContentExample example);

    JdContent selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdContent record, @Param("example") JdContentExample example);

    int updateByExample(@Param("record") JdContent record, @Param("example") JdContentExample example);

    int updateByPrimaryKeySelective(JdContent record);

    int updateByPrimaryKey(JdContent record);
}