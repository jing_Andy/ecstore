package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdSpecificationOption;
import com.wj.ecstore.pojo.JdSpecificationOptionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdSpecificationOptionMapper {
    int countByExample(JdSpecificationOptionExample example);

    int deleteByExample(JdSpecificationOptionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdSpecificationOption record);

    int insertSelective(JdSpecificationOption record);

    List<JdSpecificationOption> selectByExample(JdSpecificationOptionExample example);

    JdSpecificationOption selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdSpecificationOption record, @Param("example") JdSpecificationOptionExample example);

    int updateByExample(@Param("record") JdSpecificationOption record, @Param("example") JdSpecificationOptionExample example);

    int updateByPrimaryKeySelective(JdSpecificationOption record);

    int updateByPrimaryKey(JdSpecificationOption record);
}