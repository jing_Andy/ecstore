package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdItemCat;
import com.wj.ecstore.pojo.JdItemCatExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdItemCatMapper {
    int countByExample(JdItemCatExample example);

    int deleteByExample(JdItemCatExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdItemCat record);

    int insertSelective(JdItemCat record);

    List<JdItemCat> selectByExample(JdItemCatExample example);

    JdItemCat selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdItemCat record, @Param("example") JdItemCatExample example);

    int updateByExample(@Param("record") JdItemCat record, @Param("example") JdItemCatExample example);

    int updateByPrimaryKeySelective(JdItemCat record);

    int updateByPrimaryKey(JdItemCat record);
}