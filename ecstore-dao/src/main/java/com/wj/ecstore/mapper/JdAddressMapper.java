package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdAddress;
import com.wj.ecstore.pojo.JdAddressExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdAddressMapper {
    int countByExample(JdAddressExample example);

    int deleteByExample(JdAddressExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JdAddress record);

    int insertSelective(JdAddress record);

    List<JdAddress> selectByExample(JdAddressExample example);

    JdAddress selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JdAddress record, @Param("example") JdAddressExample example);

    int updateByExample(@Param("record") JdAddress record, @Param("example") JdAddressExample example);

    int updateByPrimaryKeySelective(JdAddress record);

    int updateByPrimaryKey(JdAddress record);
}