package com.wj.ecstore.mapper;

import com.wj.ecstore.pojo.JdSeller;
import com.wj.ecstore.pojo.JdSellerExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JdSellerMapper {
    int countByExample(JdSellerExample example);

    int deleteByExample(JdSellerExample example);

    int deleteByPrimaryKey(String sellerId);

    int insert(JdSeller record);

    int insertSelective(JdSeller record);

    List<JdSeller> selectByExample(JdSellerExample example);

    JdSeller selectByPrimaryKey(String sellerId);

    int updateByExampleSelective(@Param("record") JdSeller record, @Param("example") JdSellerExample example);

    int updateByExample(@Param("record") JdSeller record, @Param("example") JdSellerExample example);

    int updateByPrimaryKeySelective(JdSeller record);

    int updateByPrimaryKey(JdSeller record);
}